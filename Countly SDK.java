package <replace this with your own package name>;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by linhtt on 15/10/2017.
 */

public class CountlySDK {
    private static CountlySDK singleton;
    private CountlySDK(){}

    private RequestQueue rQueue;
    private String serverAddr;
    private String appKey;
    private String deviceId;
    private HashMap<String,String> baseParamsM;
    private String baseParams;

    public void init(Context context, String serverAddr, String appKey){
        this.init(context,serverAddr,appKey, "some undefined deviceID");
    }

    public void init(Context context, String serverAddr, String appKey, String deviceId){
        this.rQueue = Volley.newRequestQueue(context);
        this.serverAddr = serverAddr;
        this.appKey = appKey;
        this.deviceId = deviceId;
        baseParamsM = new HashMap<>();
        baseParamsM.put("app_key", appKey);
        baseParamsM.put("device_id", deviceId);
        baseParams = urlParamsSerialize(baseParamsM);
    }

    public void init(Context context, String serverAddr, String appKey, String ifNullUseOtherId, String otherId){
        if(ifNullUseOtherId == null){
            this.init(context, serverAddr, appKey, otherId);
        }
        this.init(context, serverAddr, appKey, ifNullUseOtherId);
    }

    public void changeDeviceId(String deviceId){
        throw new UnsupportedOperationException();
    }

    public void recordEvent(String key, int count, int sum){
        throw new UnsupportedOperationException();
    }

    public void recordEvent(String key, HashMap<String, String> segmentation){
        this.recordEvent(key,segmentation,1);
    }

    public void recordEvent(String key, HashMap<String, String> segmentation, int count){
        StringBuilder fullUrl = new StringBuilder(serverAddr);
        fullUrl.append("/i?");

        HashMap<String, String> params = new HashMap<>();
        params.put("events", eventSerialise(key, segmentation, count));

        fullUrl.append(urlParamsSerialize(params));
        fullUrl.append(baseParams);


        rQueue.add(new StringRequest(
            Request.Method.GET,
            fullUrl.toString(),
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
        ));
    }

    public void recordEvent(String key, HashMap<String, String> segmentation, int count, int sum){
        throw new UnsupportedOperationException();
    }

    public void recordEvent(String key, HashMap<String, String> segmentation, int count, int sum, int dur){
        throw new UnsupportedOperationException();
    }

    public static CountlySDK sharedInstance(){
        if(singleton == null){
            singleton = new CountlySDK();
        }
        return CountlySDK.singleton;
    }

    private String ENCODING = "utf-8";
    private String urlParamsSerialize(HashMap<String, String> params){
        StringBuilder fullUrl = new StringBuilder();
        try {
            for(Map.Entry<String, String> e : params.entrySet()){
                fullUrl.append(URLEncoder.encode(e.getKey(), ENCODING));
                fullUrl.append("=");
                fullUrl.append(URLEncoder.encode(e.getValue(), ENCODING));
                fullUrl.append("&");
            }
            return fullUrl.toString();
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: " + ENCODING, uee);
        }
    }

    private String eventSerialise(String key, HashMap<String, String> segmentation, int count){
        StringBuilder res = new StringBuilder();
        res.append("[{");
        res.append("\"key\":");
        res.append("\""+ key + "\",");
        res.append("\"count\":");
        res.append(((count<1)?1:count));

        res.append(",\"segmentation\":{");

        Iterator<Map.Entry<String, String>> it = segmentation.entrySet().iterator();
        Map.Entry<String, String> current = it.next();
        res.append("\"");
        res.append(current.getKey());
        res.append("\":\"");
        res.append(current.getValue());
        res.append("\"");
        while(it.hasNext()){
            current = it.next();
            res.append(",");
            res.append("\"");
            res.append(current.getKey());
            res.append("\":\"");
            res.append(current.getValue());
            res.append("\"");
        }
        res.append("}");

        res.append("}]");
        return res.toString();
    }

}
